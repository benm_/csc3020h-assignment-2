// Window.h
#ifndef __WINDOW_H_
#define __WINDOW_H_

#include <string.h>

namespace mrxben001
{

    class window
    {
    public:
        window() {}
        window(int w, int h, const char * t)
        {
            width = w;
            height = h;
            int len = strlen(t);
            title = new char[len+1];
            strcpy(title, t);
        }

        int width;
        int height;
        char * title;
    };

}
#endif
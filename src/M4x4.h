#ifndef _M4x4_H_
#define _M4x4_H_ 

/*
* This class represents a 4x4 matrix and allows one to store transformations in it. This is used to help stop
* the effects of gimbal lock and to ensure global and local transformation order.
*/

namespace mrxben001
{
    class M4x4
    {
    public:
        // CONSTRUCT - load identity matrix
        M4x4()
        {
            load_identity();
        }
        
        /*
        * 1 0 0 0 
        * 0 1 0 0 
        * 0 0 1 0
        * 0 0 0 1
        */
        void load_identity()
        {
            memset( value, 0, sizeof(value) );
            value[0]=value[5]=value[10]=value[15]=1;
        }

        // Multiply a translate matrix by the current matrix
        void translatef(float x, float y, float z)
        {
            glPushMatrix();                         
                glLoadIdentity();                               // start with identity
                glTranslatef(x, y, z);                          // add the current rotation
                glMultMatrixf(value);                           // multiply in the rotmatix
                glGetFloatv( GL_MODELVIEW_MATRIX, value );      // load it into the model matrix
            glPopMatrix();
        }

        // Multiply a rotate matrix by the current matrix
        void rotatef(float angle, float x, float y, float z)
        {
             glPushMatrix();                         
                glLoadIdentity();                               // start with identity
                glRotatef( angle, x,y,z );                      // add the current rotation
                glMultMatrixf(value);                           // multiply in the rotmatix
                glGetFloatv( GL_MODELVIEW_MATRIX, value );      // load it into the model matrix
            glPopMatrix();
        }

        void rotatef(float angle, const float* axis)
        {
             rotatef(angle, axis[0], axis[1], axis[2]);
        }

        // Multiply the current matrix with rotation matrix
        void rotatef_after(float angle, float x, float y, float z)
        {
             glPushMatrix();                         
                glLoadIdentity();                               // start with identity
                glMultMatrixf(value);                           // multiply in the rotmatix
                glRotatef( angle, x,y,z );                      // add the current rotation
                glGetFloatv( GL_MODELVIEW_MATRIX, value );      // load it into the model matrix
            glPopMatrix();
        }

        void rotatef_after(float angle, const float* axis)
        {
             rotatef_after(angle, axis[0], axis[1], axis[2]);
        }

        void multMatrixf()
        {
            glMultMatrixf(value);
        }

    private:
        float value[16];
    };
}

#endif
// Main.cpp
#include "main.h"

using namespace std;

// CONSTANTS ==========================

const int XAXIS = 0x8000;
const int YAXIS = 0x8001;
const int ZAXIS = 0x8002;

const float XAXISfv[] = {1.0,0.0,0.0};
const float YAXISfv[] = {0.0,1.0,0.0};
const float ZAXISfv[] = {0.0,0.0,1.0};

const float RED[]   = {1.0,0.0,0.0};
const float GREEN[] = {0.0,1.0,0.0};
const float BLUE[]  = {0.0,0.0,1.0};

const float PI = 3.14159265;

// GLOBALS ============================
mrxben001::window win; 

mrxben001::M4x4 local;    
mrxben001::M4x4 global;     

float cameradistance = 6.0;
float camerayaw = PI/4;
float camerapitch = PI/4;    
float camerapos[3];

// The selected axis 0-x 1-y 2-z
int selected_axis = XAXIS;

bool leftbutton = false;
bool rightbutton = false;
float mouse_click_pos[2];


/* MAIN METHOD */
int main(int argc, char* argv[])
{
    // Initialise GLUT and extract cmd options
    glutInit(&argc,argv);

    // Set display mode: RGBA, double buffered, depth buffer
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

    win = mrxben001::window(800, 600, "MRXBEN001 Prac 2"); 

    // Set window size
    glutInitWindowSize(win.width, win.height);

    // Set window position centered-ish
    glutInitWindowPosition(200, (glutGet(GLUT_SCREEN_HEIGHT)-win.height)/2);

    glutCreateWindow(win.title);

    //displays the objects for the first time
    glutDisplayFunc(&drawWindow);

     glutMotionFunc( &motionFunc ); 
    glutMouseFunc( &mouseFunc ); 

    //displays the objects(updates)
    glutIdleFunc(&drawWindow);

    // resize action
    glutReshapeFunc(&resize);

    glutKeyboardFunc(&keyPress);

    initWindow();

    calc_camera();

    //main loop
    glutMainLoop();


    return 0;
}

/*
* INITWINDOW
* Setup OpenGL variables and quantities
*
* SOME GLOBALS */
GLfloat amb_light[] = { 0.1, 0.1, 0.1, 1.0 };
GLfloat diffuse[] = { 0.5, 0.5, 0.5, 1 };
GLfloat light_position[] = { 2.0, 2.0, 2.0, 1 };
GLfloat specular[] = { 1, 1, 1 , 1 };
GLfloat specReflection[] = { 0.8, 0.8, 0.8, 1.0 };

void initWindow()
{
    // We want to render on the full screen
    glViewport(0, 0, win.width, win.height);


    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);       

    // ======== SETUP LIGHTING =======
    glEnable(GL_LIGHTING);

    // Globale light model
    glLightModelfv( GL_LIGHT_MODEL_AMBIENT, amb_light );
    glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE );

    // LIGHT 0
    glEnable(GL_LIGHT0); 
    glLightfv( GL_LIGHT0, GL_POSITION, light_position);
    glLightfv( GL_LIGHT0, GL_DIFFUSE, diffuse );
    glLightfv( GL_LIGHT0, GL_SPECULAR, specular );

    // ========== TEXTURING ==========
    glEnable( GL_COLOR_MATERIAL );
    glMaterialf(GL_FRONT, GL_SHININESS, 64.0);
    glMaterialfv(GL_FRONT, GL_SPECULAR, specReflection);

    // ==== SETUP PROJECTION MODE ====
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();                                                              //clear

    gluPerspective(45, ((GLfloat) win.width / win.height), 1.0f, 500.0f);

    glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );   

    // ====== CLEAR MATRIX MODE ======
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

/*
* RESIZE
* called when window is resized.
*/
void resize(int w, int h)
{
    win.width = w;
    win.height = h;
    glViewport(0, 0, win.width, win.height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, ((GLfloat) win.width / win.height), 1.0f, 500.0f);   // new perspective
    glMatrixMode(GL_MODELVIEW);
}

/*
* DRAWHUD
* draws the hud ontop of everything. This is a bit complicated as it first needs 
* to change the projection matrix into an orthographic one
*/
void drawHud()
{
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
        glLoadIdentity();
        glOrtho(0.0f, win.width, win.height, 0.0f, -1.0f, 10.0f);
        
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
            glLoadIdentity();

            glDisable(GL_LIGHTING);
            glDisable(GL_DEPTH_TEST);

            // Helps when spacing the lines
            int line = 1;
            int lineheight = 14;

            glColor4f(1.0f, 1.0f, 1.0f,1.0f);
            drawText("CONTROLS:", 10, lineheight*line); line++;
            drawText("============", 10, lineheight*line); line++;

            drawText("Current Axis:", 10, lineheight*line);

            if(selected_axis==XAXIS)
                drawText("X", 100, lineheight*line);
            else if(selected_axis==YAXIS)
                drawText("Y", 100, lineheight*line);
            else if(selected_axis==ZAXIS)
                drawText("Z", 100, lineheight*line);

            line++;


            drawText("Change Rotation Axis:    [R]", 10, lineheight*line); line++; 

            line++;

            drawText("Rotate: [Q] or [E]", 10, lineheight*line++);

            line++;

            drawText("Move along X axis:       (+) [X]   (-) [Shift X]", 10, lineheight*line++);
            drawText("Move along Y axis:       (+) [Y]   (-) [Shift Y]", 10, lineheight*line++);
            drawText("Move along Z axis:       (+) [Z]   (-) [Shift Z]", 10, lineheight*line++);

            line++;
            drawText("Rotate Camera: Left mouse button", 10, lineheight*line++);
            drawText("Zoom Camera: Right mouse button", 10, lineheight*line++);

            glEnable(GL_DEPTH_TEST);
            glEnable(GL_LIGHTING);

        glPopMatrix();
        glMatrixMode(GL_PROJECTION);

    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
}

/*
* DRAWAXISV
* Draw an axis on the given vector with the given colour, width and length
*/
void drawAxisv(const float* v, const float* c, float width, float length)
{
    glDisable(GL_LIGHTING);
    glLineWidth(width); 
    glColor3f(c[0], c[1], c[2]);
    glBegin(GL_LINES);
    glVertex3f(v[0]*length, v[1]*length, v[2]*length);
    glVertex3f(-v[0]*length, -v[1]*length, -v[2]*length);
    glEnd();
    glEnable(GL_LIGHTING);
}

/*
* DRAWWINDOW
* Draw the screen
*/
void drawWindow()
{
    //clears colour and depth buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();

    // Add the camera matrix transform to the matrix stack.
    //  basically the camera stays at 0,0,0 and all objects are inversely transformed with respect to the camera

    gluLookAt( camerapos[0],camerapos[1],camerapos[2], 0,0,0, 0,1,0);    


    glLightfv( GL_LIGHT0, GL_POSITION, light_position);
    // ==== DRAW WORLD AXIS ====

    drawAxisv(XAXISfv, RED,   1.0, 15);
    drawAxisv(YAXISfv, GREEN, 1.0, 15);
    drawAxisv(ZAXISfv, BLUE,  1.0, 15);


    glPushMatrix();            
 
        local.multMatrixf();

        drawAxisv(XAXISfv, RED,   (selected_axis==XAXIS)?5:1.0, 1.8);
        drawAxisv(YAXISfv, GREEN, (selected_axis==YAXIS)?5:1.0, 1.8);
        drawAxisv(ZAXISfv, BLUE,  (selected_axis==ZAXIS)?5:1.0, 1.8);

        global.multMatrixf();

        // Draw the teapot
        glColor3f(1.0, 1.0, 1.0);
        glutSolidTeapot(1);

    glPopMatrix();  

    drawHud();    

    //swaps the double buffer
    glutSwapBuffers();
}

/*
* DRAWTEXT
* draw a char array at the given position on the screen
*/
void drawText(char *text, int x, int y)
{
    glRasterPos2f(x,y);
    char *c;
    for (c=text; *c != '\0'; c++) 
    {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *c);
    }
}

/*
* KEYPRESS
* Called when a key is pressed
*/
void keyPress(unsigned char key, int x, int y)
{

    if (key == 'r')
    {
        selected_axis = (selected_axis-XAXIS+1) % 3 + XAXIS;
    }
    else if(key == 'q')
    {
        switch(selected_axis)
        {
            case XAXIS:
                global.rotatef(1.0f, XAXISfv);
                break;
            case YAXIS:
                global.rotatef(1.0f, YAXISfv);
                break;
            case ZAXIS:
                global.rotatef(1.0f, ZAXISfv);
                break;
            default:
                break;
        }
    }
    else if(key == 'e')
    {
        switch(selected_axis)
        {
            case XAXIS:
                global.rotatef(-1.0f, XAXISfv);
                break;
            case YAXIS:
                global.rotatef(-1.0f, YAXISfv);
                break;
            case ZAXIS:
                global.rotatef(-1.0f, ZAXISfv);
                break;
            default:
                break;
        }
    }    
    else if(key == 'x')
    {
        local.translatef(0.1, 0.0, 0.0);
    }
    else if(key == 'X')
    {
        local.translatef(-0.1, 0.0, 0.0);
    }
    else if(key == 'y')
    {
        local.translatef(0.0, 0.1, 0.0);
    }
    else if(key == 'Y')
    {
        local.translatef(0.0, -0.1, 0.0);
    }
    else if(key == 'z')
    {
        local.translatef(0.0, 0.0, 0.1);
    }
    else if(key == 'Z')
    {
        local.translatef(0.0, 0.0, -0.1);
    }
}


/*
* MOUSEFUNC
* Called when a mouse button is pressed
*/
void mouseFunc(int button, int state, int x, int y)
{
    mouse_click_pos[0] = x; 
    mouse_click_pos[1] = y; 
    leftbutton = (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN);
    rightbutton = (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN);
}

/*
* MOTIONFUNC
* Called when the mouse is moved on the window while a button is pressed
*/
void motionFunc(int x, int y)
{   
    // Left button controls spherical movement
    if( leftbutton ) 
    { 
        camerayaw =   ( camerayaw + ( GLfloat ) ( x - mouse_click_pos[0] ) / 300.0 ) ; 
        camerapitch = ( camerapitch + ( GLfloat ) ( mouse_click_pos[1] - y ) / 300.0 );

        // clamp
        if (camerapitch < 0.001) camerapitch = 0.001;
        if (camerapitch > (PI-0.001)) camerapitch = PI-0.001;
    }

    // Right button controls zoom
    if( rightbutton )
    {        
        cameradistance = (cameradistance + (GLfloat) (mouse_click_pos[1] - y) / 100);

        // clamp
        if (cameradistance < 3) cameradistance = 3;
        if (cameradistance > 30) cameradistance = 30;
    }

    // Store current position
    mouse_click_pos[0] = x; 
    mouse_click_pos[1] = y; 

    // Update
    calc_camera();
    glutPostRedisplay(); 
}

/*
* CALC_CAMERA
* Convert the camera's spherical coordinates to cartesian coordinates
*/
void calc_camera()
{
    camerapos[0] = cameradistance*sin(camerapitch)*cos(camerayaw);
    camerapos[2] = cameradistance*sin(camerapitch)*sin(camerayaw);
    camerapos[1] = cameradistance*cos(camerapitch);
}
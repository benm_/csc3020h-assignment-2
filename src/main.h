// Main.h
#ifndef __MAIN_H__
#define __MAIN_H__

#include <stdlib.h>
#include <GL/glut.h>
#include <iostream>
#include <math.h>

#include "window.h"
#include "M4x4.h"

void initWindow();
void resize(int w, int h);
void drawWindow();
void drawText(char *text, int x, int y);
void keyPress(unsigned char key, int x, int y);
void mouseFunc(int button, int state, int x, int y);
void motionFunc(int x, int y);
void calc_camera();

#endif